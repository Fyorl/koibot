'use strict';

require('chai').use(require('chai-as-promised')).should();

const Collection = require('discord.js').Collection;
const integration_context = require('../integration.mock');
const log_plugin = require('../../plugins/log/plugin');

describe('Log plugin', function () {
	let context;

	beforeEach(async function () {
		context = await integration_context();
		await context.plugin_loader.register_plugin('log');
		await context.db.migrate(1, 'plugins/log/schema');
	});

	afterEach(async function () {
		await context.db.close();
	});

	describe('#handle_message', function () {
		it('handles direct messages', async function () {
			const plugin = new log_plugin(context);
			plugin.enabled = true;

			await plugin.handle_message({
				id: 'm360085015838851073'
				, author: {id: 'a360085015838851073'}
				, channel: {id: 'c360085015838851073', type: 'dm'}
				, createdTimestamp: 1506856935048
				, content: 'hey <@a360085015838851073>'
				, cleanContent: 'hey @you'
				, mentions: {users: new Collection(), roles: new Collection()}
			});

			const messages = await context.db.all('SELECT * FROM Messages');
			messages.should.deep.equal([{
				message_id: 1
				, dc_message_id: 'm360085015838851073'
				, dc_channel_id: 'c360085015838851073'
				, channel_name: null
				, dc_server_id: null
				, server_name: null
				, content_raw: 'hey <@a360085015838851073>'
				, content_clean: 'hey @you'
				, dc_author_id: 'a360085015838851073'
				, direct: 1
				, time: 1506856935048
			}]);
		});

		it('handles mentions', async function () {
			const plugin = new log_plugin(context);
			plugin.enabled = true;

			await plugin.handle_message({
				id: 'm360085015838851073'
				, author: {id: 'a360085015838851073'}
				, channel: {
					id: 'c360085015838851073'
					, type: 'text'
					, name: 'chan'
					, guild: {id: 'g360085015838851073', name: 'serv'}
				}
				, createdTimestamp: 1506856935048
				, content: 'hey <@&r360085015838851073> '
					+ '<@a360085015838851073> <@a360085015838851074>'
				, cleanContent: 'hey @role @a @b'
				, mentions: {
					users: new Collection([
						['a360085015838851073', {}]
						, ['a360085015838851074', {}]
					])
					, roles: new Collection([['r360085015838851073', {}]])
				}
			});

			const messages = await context.db.all('SELECT * FROM Messages');
			messages.should.deep.equal([{
				message_id: 1
				, dc_message_id: 'm360085015838851073'
				, dc_channel_id: 'c360085015838851073'
				, channel_name: 'chan'
				, dc_server_id: 'g360085015838851073'
				, server_name: 'serv'
				, content_raw: 'hey <@&r360085015838851073> '
					+ '<@a360085015838851073> <@a360085015838851074>'
				, content_clean: 'hey @role @a @b'
				, dc_author_id: 'a360085015838851073'
				, direct: 0
				, time: 1506856935048
			}]);

			const mentions = await context.db.all('SELECT * FROM Mentions');
			mentions.should.deep.equal([
				{mention_id: 1, message_id: 1, dc_mention_id: 'a360085015838851073', type: 'user'}
				, {mention_id: 2, message_id: 1, dc_mention_id: 'a360085015838851074', type: 'user'}
				, {mention_id: 3, message_id: 1, dc_mention_id: 'r360085015838851073', type: 'role'}
			]);
		});
	});
});
