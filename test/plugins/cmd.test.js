'use strict';

require('chai').use(require('chai-as-promised')).should();

const stub = require('../mock').stub;
const mock_context = require('../context.mock');
const cmd_plugin = require('../../plugins/cmd/plugin');

describe('Command plugin', function () {
	describe('#parse_args', function () {
		it('handles an empty string', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			plugin.parse_args('').should.have.lengthOf(0);
		});

		it('simplifies arguments correctly', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			plugin.parse_args('cmd arg1 arg2 arg3').simplify().should.deep.equal(
				['cmd', 'arg1', 'arg2', 'arg3']);
		});

		it('correctly parses until the given number', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			plugin.parse_args('quote user a very memorable quote', 2).simplify().should.deep.equal(
				['quote', 'user', 'a very memorable quote']);
		});

		it('parses command arguments correctly', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			const command = 'say <@!123> <#123> <@&123> abc def "1 2" "1 \\"2\\" 3" 1\\ 2 \\\\1 '
				+ '<@&abc> <def> "<@123>" \\<@123> "abc def';
			Array.from(plugin.parse_args(command)).should.deep.equal([
				{text: 'say'}
				, {type: 'user', id: '123', data: {}}
				, {type: 'chan', id: '123', data: {}}
				, {type: 'role', id: '123', data: {}}
				, {text: 'abc'}
				, {text: 'def'}
				, {text: '1 2'}
				, {text: '1 "2" 3'}
				, {text: '1 2'}
				, {text: '\\1'}
				, {text: '<abc>'}
				, {text: '<def>'}
				, {text: '<@123>'}
				, {text: '<@123>'}
				, {text: 'abc def'}
			]);
		});
	});

	describe('#handle_message', function () {
		it('ignores empty messages', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			context.events = {emit: stub()};
			plugin.enabled = true;
			plugin.handle_message({content: ''});
			context.events.emit.invocations().should.equal(0);
		});

		it('ignores non-commands', function () {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			context.config.command_symbols = ['!'];
			context.events = {emit: stub()};
			plugin.enabled = true;
			plugin.handle_message({content: 'cmd'});
			context.events.emit.invocations().should.equal(0);
		});

		it('handles single word commands', function (done) {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			const message = {content: '!cmd', mentions: {users: new Map(), roles: new Map()}};
			context.config.command_symbols = ['!'];

			context.events.on('command_cmd', (msg, line, args) => {
				msg.should.equal(message);
				args.should.have.lengthOf(0);
				line.should.have.lengthOf(0);
				done();
			});

			plugin.enabled = true;
			plugin.handle_message(message);
		});

		it('handles single argument commands', function (done) {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			context.config.command_symbols = ['!'];

			context.events.on('command_cmd', (msg, line, args) => {
				Array.from(args).should.deep.equal([{text: 'arg1'}]);
				line.should.equal('arg1');
				done();
			});

			plugin.enabled = true;
			plugin.handle_message({
				content: '!cmd arg1'
				, mentions: {users: new Map(), roles: new Map()}
			});
		});

		it('matches mentions', function (done) {
			const context = mock_context();
			const plugin = new cmd_plugin(context);
			context.config.command_symbols = ['!'];

			context.events.on('command_cmd', (msg, line, args) => {
				Array.from(args).should.deep.equal([
					{type: 'chan', id: '123', data: {}}
					, {type: 'user', id: '123', data: {id: '123'}}
					, {type: 'role', id: '123', data: {chan: '456'}}
					, {type: 'user', id: '456', data: {id: '456'}}
					, {text: 'stuff'}
				]);
				done();
			});

			plugin.enabled = true;
			plugin.handle_message({
				content: '!cmd <#123> <@!123> <@&123> <@!456> stuff'
				, mentions: {
					users: new Map([['123', {id: '123'}], ['456', {id: '456'}]])
					, roles: new Map([['123', {chan: '456'}]])
				}
			});
		});
	});
});
