'use strict';

require('chai').use(require('chai-as-promised')).should();
const expect = require('chai').expect;

const Collection = require('discord.js').Collection;
const stub = require('../mock').stub;
const mock = require('../mock').mock;
const mock_resolves = require('../mock').mock_resolves;
const integration_context = require('../integration.mock');
const log_plugin = require('../../plugins/log/plugin');
const quote_plugin = require('../../plugins/quote/plugin');

let current_time = 1;
const create_message = override => {
	const base = {
		id: `m${current_time}`
		, author: {id: 'a1'}
		, channel: {id: 'c1', type: 'text', name: 'chan'}
		, createdTimestamp: current_time
		, content: 'I am a [find me] quote'
		, cleanContent: 'I am a [find me] quote'
		, mentions: {users: new Collection(), roles: new Collection()}
	};

	current_time += 1;
	return Object.override(base, override);
};

const create_quote_row = override => {
	const base = {
		quote_id: 1
		, dc_message_id: 'm2'
		, dc_channel_id: 'c1'
		, channel_name: 'chan'
		, dc_server_id: null
		, server_name: null
		, content_raw: 'I am a [find me] quote'
		, content_clean: 'I am a [find me] quote'
		, dc_quoter_id: 'a2'
		, dc_quotee_id: 'a1'
		, time_posted: 2
		, time_quoted: 3
		, direct: 0
	};

	return Object.override(base, override);
};

describe('Quote plugin', function () {
	let context, log, quote;

	beforeEach(async function () {
		current_time = 1;
		context = await integration_context();
		context.config.command_symbols = ['!'];
		context.util.is_blacklisted = mock_resolves(() => false);
		await context.plugin_loader.register_plugin('log');
		await context.plugin_loader.register_plugin('quote');
		await context.db.migrate(1, 'plugins/log/schema');
		await context.db.migrate(2, 'plugins/quote/schema');

		log = new log_plugin(context);
		quote = new quote_plugin(context);
		log.enabled = true;
		quote.enabled = true;
	});

	afterEach(async function () {
		await context.db.close();
	});

	describe('#quote', function () {
		it('finds the most recent quote', async function () {
			await log.handle_message(create_message());
			await log.handle_message(create_message());
			await log.handle_message(create_message({channel: {id: 'c2', type: 'dm'}}));

			const msg = {
				author: {id: 'a2'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(msg, '', []);
			msg.channel.send.last_invocation_args().should.deep.equal(['Added quote #1.']);

			const rows = await context.db.all('SELECT * FROM Quotes');
			rows.should.deep.equal([
				create_quote_row({time_quoted: 4})
			]);
		});

		it('finds the most recent quote by the mentioned user', async function () {
			await log.handle_message(create_message());
			await log.handle_message(create_message({author: {id: 'a2'}}));

			const msg = {
				author: {id: 'a3'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(msg, '', [{type: 'user', id: 'a1'}]);
			msg.channel.send.last_invocation_args().should.deep.equal(['Added quote #1.']);

			const rows = await context.db.all('SELECT * FROM Quotes');
			rows.should.deep.equal([
				create_quote_row({dc_message_id: 'm1', dc_quoter_id: 'a3', time_posted: 1})
			]);
		});

		it('finds the most recent quote containing the search phrase', async function () {
			await log.handle_message(create_message());
			await log.handle_message(create_message({content: '!quote find me'}));
			await log.handle_message(create_message({content: 'I am a quote'}));

			const msg = {
				author: {id: 'a2'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(msg, 'find me', [{text: 'find'}, {text: 'me'}]);
			msg.channel.send.last_invocation_args().should.deep.equal(['Added quote #1.']);

			const rows = await context.db.all('SELECT * FROM Quotes');
			rows.should.deep.equal([
				create_quote_row({dc_message_id: 'm1', time_posted: 1, time_quoted: 4})
			]);
		});

		it('finds the most recent quote by the given user '
			+ 'that contains the whole search phrase'
		, async function () {
			await log.handle_message(create_message({author: {id: 'a2'}}));
			await log.handle_message(create_message({content: 'I am a quote'}));
			await log.handle_message(create_message());

			const msg = {
				author: {id: 'a3'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(
				msg
				, '<@a2> find me'
				, [{type: 'user', id: 'a2'}, {text: 'find'}, {text: 'me'}]);

			msg.channel.send.last_invocation_args().should.deep.equal(['Added quote #1.']);
			const rows = await context.db.all('SELECT * FROM Quotes');
			rows.should.deep.equal([
				create_quote_row({
					dc_message_id: 'm1'
					, dc_quoter_id: 'a3'
					, dc_quotee_id: 'a2'
					, time_posted: 1
					, time_quoted: 4
				})
			]);
		});

		it('copies mentions', async function () {
			await log.handle_message(
				create_message({
					mentions: {
						users: new Collection([['a3', {}]])
						, roles: new Collection([['r1', {}]])
					}
					, content: 'hey <@a3> I mentioned you'
				}));

			const msg = {
				author: {id: 'a2'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(msg, 'hey <@a3>', [{text: 'hey'}, {type: 'user', id: 'a3'}]);
			msg.channel.send.last_invocation_args().should.deep.equal(['Added quote #1.']);

			const rows = await context.db.all('SELECT * FROM QuoteMentions');
			rows.should.deep.equal([
				{quote_mention_id: 1, quote_id: 1, dc_mention_id: 'a3', type: 'user'}
				, {quote_mention_id: 2, quote_id: 1, dc_mention_id: 'r1', type: 'role'}
			]);
		});

		it('does not quote the same message twice', async function () {
			await log.handle_message(create_message());
			await log.handle_message(create_message());

			const msg = {
				author: {id: 'a2'}
				, channel: {id: 'c1', send: stub()}
				, createdTimestamp: current_time
			};

			await quote.handle_quote(msg, '', []);
			await quote.handle_quote(msg, '', []);

			msg.channel.send.last_invocation_args().should.deep.equal(['Already quoted.']);
		});
	});

	describe('#findquote', function () {
		it('ignores non-number IDs', async function () {
			context.db.get = stub();
			await quote.handle_findquote({}, '', [{text: '#tight'}]);
			context.db.get.invocations().should.equal(0);
		});

		it('fetches the quote by ID', async function () {
			const msg = {channel: {send: stub()}, guild: {members: {}}};
			msg.guild.members.fetch = mock_resolves(user_id => {
				return {
					nickname: user_id,
					displayAvatarURL: () => `https://discord.js/${user_id}.jpg`
				};
			});

			await context.db.insert('Quotes', create_quote_row());
			await quote.handle_findquote(msg, '', [{text: '#1'}]);

			const embed = msg.channel.send.last_invocation_args()[0].embeds[0];
			embed.author.should.deep.equal({
				icon_url: 'https://discord.js/a1.jpg', name: 'a1', url: undefined
			});
			embed.footer.should.deep.equal({
				icon_url: 'https://discord.js/a2.jpg', text: 'Quoted by @a2'
			});
			embed.title.should.equal('#1');
			embed.description.should.equal('I am a [find me] quote');
			embed.timestamp.valueOf().should.equal(new Date(2).toISOString());
			msg.guild.members.fetch.history().should.deep.equal([['a2'], ['a1']]);
		});
	});

	describe('#randomquote', function () {
		it('retrieves a random quote by a user containing the search phrase', async function () {
			const msg = {channel: {send: stub()}, guild: {id: 'g1', members: {}}};
			context.util.get_random_int = mock(() => 1);
			msg.guild.members.fetch = mock_resolves(user_id => {
				return {nickname: user_id, avatarURL: `${user_id}.jpg`};
			});

			let saved_quote_row = {};
			const original_fn = quote.display_quote;
			quote.display_quote = async (msg, quote_row) => {
				saved_quote_row = quote_row;
				return original_fn(msg, quote_row);
			};

			const rows = [
				create_quote_row({dc_quotee_id: 'a3', dc_server_id: 'g1'})
				, create_quote_row({quote_id: 2, dc_message_id: 'm3', dc_server_id: 'g1'})
				, create_quote_row({quote_id: 3, dc_message_id: 'm4', dc_server_id: 'g1'})
				, create_quote_row({quote_id: 4, dc_message_id: 'm5', dc_server_id: 'g1'})
				, create_quote_row({quote_id: 5, dc_message_id: 'm6', dc_server_id: 'g2'})
			];

			await context.db.insert_batch('Quotes', rows);
			await quote.handle_randomquote(
				msg
				, '<@a1> find me'
				, [{type: 'user', id: 'a1'}, {text: 'find'}, {text: 'me'}]);


			context.util.get_random_int.last_invocation_args().should.deep.equal([0, 2]);
			saved_quote_row.quote_id.should.equal(3);
		});
	});

	describe('#removequote', function () {
		it('removes a quote if you are an admin', async function () {
			const msg = {channel: {send: stub()}};
			context.config.is_admin = () => true;

			await context.db.insert('Quotes', create_quote_row());
			await context.db.insert('Quotes', create_quote_row({quote_id: 2, dc_message_id: 'm3'}));
			await quote.handle_removequote(msg, '#1', [{text: '#1'}]);

			expect(await context.db.get('SELECT * FROM Quotes WHERE quote_id = 1')).to.be.undefined;
			(await context.db.all('SELECT * FROM Quotes')).should.have.lengthOf(1);
			msg.channel.send.last_invocation_args().should.deep.equal(['Removed quote #1.']);
		});

		it('removes a quote if you quoted it', async function () {
			const msg = {channel: {send: stub()}, author: {id: 'a3'}};
			context.config.is_admin = () => false;

			await context.db.insert('Quotes', create_quote_row({dc_quoter_id: 'a3'}));
			await quote.handle_removequote(msg, '#1', [{text: '#1'}]);
			(await context.db.all('SELECT * FROM Quotes')).should.have.lengthOf(0);
			msg.channel.send.last_invocation_args().should.deep.equal(['Removed quote #1.']);
		});

		it('denies your request if you did not quote it and are not an admin', async function () {
			const msg = {channel: {send: stub()}, author: {id: 'a3'}};
			context.config.is_admin = () => false;

			await context.db.insert('Quotes', create_quote_row());
			await quote.handle_removequote(msg, '#1', [{text: '#1'}]);
			(await context.db.all('SELECT * FROM Quotes')).should.have.lengthOf(1);
			msg.channel.send.last_invocation_args().should.deep.equal(['Access denied.']);
		});
	});
});
