'use strict';

require('chai').use(require('chai-as-promised')).should();

const Collection = require('discord.js').Collection;
const stub = require('../mock').stub;
const mock_resolves = require('../mock').mock_resolves;
const integration_context = require('../integration.mock');
const log_plugin = require('../../plugins/log/plugin');
const sed_plugin = require('../../plugins/sed/plugin');

let current_time = 1;
const create_message = override => {
	const base = {
		id: `m${current_time}`,
		author: {id: 'a1'},
		channel: {id: 'c1', type: 'text', name: 'chan'},
		createdTimestamp: current_time,
		content: 'Lorem ipsum dolor sit amet',
		cleanContent: 'Lorem ipsum dolor sit amet',
		mentions: {users: new Collection(), roles: new Collection()}
	};

	current_time += 1;
	return Object.override(base, override);
};

describe('Sed plugin', function () {
	let context, log, sed;

	beforeEach(async function () {
		current_time = 1;
		context = await integration_context();
		context.config.command_symbols = ['!'];
		context.util.is_blacklisted = mock_resolves(() => false);
		await context.plugin_loader.register_plugin('log');
		await context.plugin_loader.register_plugin('sed');
		await context.db.migrate(1, 'plugins/log/schema');

		log = new log_plugin(context);
		sed = new sed_plugin(context);
		log.enabled = true;
		sed.enabled = true;
	});

	afterEach(async function () {
		await context.db.close();
	});

	it('does a simple replacement', async function () {
		await log.handle_message(create_message());
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/o/a/');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "Larem ipsum dolor sit amet"']);
	});

	it('does a global replacement', async function () {
		await log.handle_message(create_message());
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/o/a/g');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "Larem ipsum dalar sit amet"']);
	});

	it('handles malformed sed expressions', async function () {
		await log.handle_message(create_message());
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/');
		await sed.handle_command(msg, 's/o');
		msg.channel.send.history().should.deep.equal([
			['<@a1> FTFY: "Lorem ipsum dolor sit amet"'],
			['<@a1> FTFY: "Lorem ipsum dolor sit amet"']
		]);
	});

	it('handles case-insensitive replacement', async function () {
		await log.handle_message(create_message());
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/l/i/ig');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "iorem ipsum doior sit amet"']);
	});

	it('handles backreferences', async function () {
		await log.handle_message(create_message());
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/(\\w+)$/[$1]/');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "Lorem ipsum dolor sit [amet]"']);
	});

	it('handles escapes', async function () {
		await log.handle_message(create_message({content: 'http://'}));
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/\\/\\//\\\\\\//');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "http:\\/"']);
	});

	it('ignores bot command lines', async function () {
		await log.handle_message(create_message());
		await log.handle_message(create_message({content: '!ignore me'}));
		const msg = {
			author: {id: 'a2'},
			channel: {id: 'c1', send: stub()}
		};

		await sed.handle_command(msg, 's/i/l/');
		msg.channel.send.last_invocation_args().should.deep.equal(
			['<@a1> FTFY: "Lorem lpsum dolor sit amet"']);
	});
});
