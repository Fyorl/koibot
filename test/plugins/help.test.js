'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock_context = require('../context.mock');
const help_plugin = require('../../plugins/help/plugin');

describe('Help plugin', function () {
	const make_args = (...args) => {
		const result = args;
		result.simplify = () => args;
		return result;
	};

	it('prints all available commands', function (done) {
		const context = mock_context();
		const plugin = new help_plugin(context);
		plugin.enabled = true;

		const msg = {
			reply: text => {
				text.should.equal(
					'Use !help **command** to get help on a specific command. '
					+ 'Currently available commands are: **help**, **quote**, **reply**, **say**');
				done();
			}
		};

		context.config.command_symbols = ['!'];
		context.plugins = [
			{help: {help: []}}
			, {enabled: false}
			, {help: {quote: []}}
			, {help: {reply: [], say: [], quote: []}}
		];

		context.events.emit('command_help', msg, '', []);
	});

	it('prints help on a specific command\'s usage', function (done) {
		const context = mock_context();
		const plugin = new help_plugin(context);
		plugin.enabled = true;

		const msg = {
			channel: {
				send: msg => {
					msg.should.equal(
						'**Usage**:\n'
						+ '\t`help *arg1* - does this with *arg1*\n'
						+ '\t`help *@mention* *arg1* - does *arg1* with *@mention*');
					done();
				}
			}
		};

		context.config.command_symbols = ['`'];

		context.plugins = [
			{enabled: false}
			, {help: {say: [], reply: []}}
			, {
				help: {
					help: [
						'!help *arg1* - does this with *arg1*'
						, '!help *@mention* *arg1* - does *arg1* with *@mention*'
					]
				}
			}
		];

		context.events.emit('command_help', msg, '', make_args('help', 'ignore', 'ignore'));
	});
});
