'use strict';

require('chai').use(require('chai-as-promised')).should();

const stub = require('../mock').stub;
const integration_context = require('../integration.mock');
const control_plugin = require('../../plugins/plugin-control/plugin');

describe('Plugin Control plugin', function () {
	let context, control;

	beforeEach(async function () {
		context = await integration_context();
		control = new control_plugin(context);
		control.enabled = true;
	});

	afterEach(async function () {
		await context.db.close();
	});

	it('denies access to non-admins', function (done) {
		const msg = {
			channel: {
				send: str => {
					str.should.equal('Access denied.');
					done();
				}
			}
		};

		context.config.is_admin = () => false;
		context.events.emit('command_disable-plugin', msg, 'cmd', [{text: 'cmd'}]);
	});

	it('refuses to disable required plugins', function (done) {
		const msg = {
			channel: {
				send: str => {
					str.should.equal('This plugin is required and cannot be disabled.');
					done();
				}
			}
		};

		context.config.is_admin = () => true;
		context.config.required_plugins = ['cmd', 'help', 'log', 'plugin-control'];
		context.events.emit('command_disable-plugin', msg, 'log', [{text: 'log'}]);
	});

	it('can enable plugins', async function () {
		const msg = {channel: {send: stub()}};
		const test_plugin = {enabled: false};
		context.config.is_admin = () => true;
		context.plugins.from_name = name => test_plugin;

		await context.db.insert('Plugins', {plugin_name: 'test', enabled: 0});
		await control.set_plugin_enablement(msg, [{text: 'test'}], true);

		msg.channel.send.last_invocation_args().should.deep.equal(["Plugin 'test' was enabled."]);
		test_plugin.enabled.should.be.true;

		(await context.db.get(
			'SELECT * FROM Plugins WHERE plugin_name = $plugin_name'
			, {$plugin_name: 'test'})).should.deep.equal({
			plugin_id: 1
			, plugin_name: 'test'
			, enabled: 1
		});
	});

	it('can disable plugins', async function () {
		const msg = {channel: {send: stub()}};
		const test_plugin = {enabled: true};
		context.config.is_admin = () => true;
		context.config.required_plugins = ['cmd', 'plugin-control'];
		context.plugins.from_name = name => test_plugin;

		await context.db.insert('Plugins', {plugin_name: 'test', enabled: 1});
		await control.set_plugin_enablement(msg, [{text: 'test'}], false);

		msg.channel.send.last_invocation_args().should.deep.equal(["Plugin 'test' was disabled."]);
		test_plugin.enabled.should.be.false;

		(await context.db.get(
			'SELECT * FROM Plugins WHERE plugin_name = $plugin_name'
			, {$plugin_name: 'test'})).should.deep.equal({
			plugin_id: 1
			, plugin_name: 'test'
			, enabled: 0
		});
	});
});
