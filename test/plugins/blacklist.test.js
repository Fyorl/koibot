'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock_resolves = require('../mock').mock_resolves;
const mock_context = require('../context.mock');
const blacklist_plugin = require('../../plugins/blacklist/plugin');

describe('Blacklist plugin', function () {
	describe('#is_blacklisted', function () {
		it('correctly reports that a user is blacklisted', async function () {
			const context = mock_context();
			context.db.get = mock_resolves(() => {return {'COUNT(*)': 1}});

			const plugin = new blacklist_plugin(context);
			const result = await plugin.is_blacklisted({author: {id: 'a1'}}, 'quote');

			result.should.be.true;
			context.db.get.last_invocation_args()[1].should.deep.equal(
				{$user_id: 'a1', $cmd: 'quote'});
		});
	});

	describe('#add_to_blacklist', function () {
		it('adds a user to the blacklist', function (done) {
			const context = mock_context();
			context.config.is_admin = () => true;
			context.db.insert_batch = mock_resolves(() => {});

			const args = [{text: 'add'}, {type: 'user', id: 'a1'}, {text: 'cmd1'}, {text: 'cmd2'}];
			const msg = {channel: {
				send: text => {
					context.db.insert_batch.last_invocation_args()[1].should.deep.equal([
						{command: 'cmd1', dc_user_id: 'a1'}
						, {command: 'cmd2', dc_user_id: 'a1'}
					]);

					text.should.equal(
						'<@a1> blacklisted from the following commands: *cmd1*, *cmd2*');

					done();
				}
			}};

			const plugin = new blacklist_plugin(context);
			plugin.enabled = true;
			context.events.emit('command_blacklist', msg, '', args);
		});
	});

	describe('#remove_from_blacklist', function () {
		it('removes a user from the blacklist', function (done) {
			const context = mock_context();
			context.config.is_admin = () => true;
			context.db.run = mock_resolves(() => {});

			const args = [
				{text: 'remove'}
				, {type: 'user', id: 'a1'}
				, {text: 'cmd1'}
				, {text: 'cmd2'}
			];

			const msg = {channel: {
				send: text => {
					context.db.run.last_invocation_args().should.deep.equal([
						'DELETE FROM Blacklist WHERE dc_user_id = $user_id '
						+ 'AND command IN ($0_cmd, $1_cmd)'
						, {$user_id: 'a1', $0_cmd: 'cmd1', $1_cmd: 'cmd2'}
					]);

					text.should.equal(
						'<@a1> removed from blacklist for the following commands: *cmd1*, *cmd2*');

					done();
				}
			}};

			const plugin = new blacklist_plugin(context);
			plugin.enabled = true;
			context.events.emit('command_blacklist', msg, '', args);
		});
	});
});
