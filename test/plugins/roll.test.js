'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock = require('../mock').mock;
const mock_resolves = require('../mock').mock_resolves;
const mock_context = require('../context.mock');
const roll_plugin = require('../../plugins/roll/plugin');

describe('Roll plugin', function () {
	it('parses and evaluates roll expressions', function (done) {
		const tests = [
			['-2d20 + 5', '-( **1** + **1** ) + **5** = **3**']
			, ['1d2 + 1d3 + 1d4', '**1** + **1** + **1** = **3**']
			, ['2d12 + 1d8 - 9', '( **1** + **1** ) + **1** - **9** = **-6**']
			, ['1d20', '**1**']
		];

		let tests_complete = 0;
		const context = mock_context();
		const plugin = new roll_plugin(context);

		plugin.enabled = true;
		context.util.get_random_int = mock(() => 1);
		context.util.is_blacklisted = mock_resolves(() => false);

		for (const test of tests) {
			const msg = {
				reply: text => {
					text.should.equal(test[1]);
					tests_complete++;

					if (tests_complete >= tests.length) {
						done();
					}
				}
			};

			context.events.emit('command_roll', msg, test[0], []);
		}
	});
});
