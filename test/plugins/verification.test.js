'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock_context = require('../context.mock');
const mock_resolves = require('../mock').mock_resolves;
const verification_plugin = require('../../plugins/verification/plugin');

describe('Verification plugin', function () {
	it('verifies a user', function (done) {
		const context = mock_context();
		context.util.is_blacklisted = mock_resolves(() => false);

		const plugin = new verification_plugin(context, {verified_role_id: 'r1'});
		plugin.enabled = true;

		const role = {};
		const add_role = mock_resolves(() => {});

		const msg = {
			author: {id: 'a1'}
			, guild: {
				available: true
				, members: {
					fetch: mock_resolves(() => {
						return {roles: {add: add_role}};
					})
				}
				, roles: {fetch: mock_resolves(() => role)}
			}
			, reply: text => {
				text.should.equal('Verified.');
				add_role.invocations().should.equal(1);
				add_role.last_invocation_args().should.deep.equal([role]);
				done();
			}
		};

		context.events.emit('command_verifyme', msg);
	});
});
