'use strict';

exports.mock = function (fn) {
	let invocations = 0;
	const history = [];

	const mock = (...args) => {
		invocations++;
		history.push(args);
		return fn(...args);
	};

	mock.invocations = () => invocations;
	mock.history = () => history;
	mock.last_invocation_args = () => history.last();
	return mock;
};

exports.mock_resolves = function (fn) {
	let invocations = 0;
	const history = [];

	const mock = (...args) => new Promise(resolve => {
		invocations++;
		history.push(args);
		resolve(fn(...args));
	});

	mock.invocations = () => invocations;
	mock.history = () => history;
	mock.last_invocation_args = () =>  history.last();
	return mock;
};

exports.mock_rejects = function (fn) {
	let invocations = 0;
	const history = [];

	const mock = (...args) => new Promise((resolve, reject) => {
		invocations++;
		history.push(args);
		reject(fn(...args));
	});

	mock.invocations = () => invocations;
	mock.history = () => history;
	mock.last_invocation_args = () => history.last();
	return mock;
};

exports.stub = () => exports.mock(() => {});
