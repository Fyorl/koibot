'use strict';

require('chai').use(require('chai-as-promised')).should();
require('../lib/util');

const mock = require('./mock').mock;
const mock_resolves = require('./mock').mock_resolves;
const mock_rejects = require('./mock').mock_rejects;

const integration_context = require('./integration.mock');
const mock_context = require('./context.mock');
const PluginLoader = require('../lib/PluginLoader');

describe('PluginLoader', function () {
	let context;

	beforeEach(async function () {
		context = await integration_context();
	});

	afterEach(async function () {
		await context.db.close();
	});

	describe('#registered_plugins', function () {
		it('returns all distinct non-koi plugins', async function () {
			await context.db.run(
				'INSERT INTO Plugins (plugin_name, enabled) VALUES ("plugin", 1), ("plugin2", 1)');
			const plugin_loader = new PluginLoader(context);
			const result = await plugin_loader.registered_plugins();
			[...result].should.deep.equal(['plugin', 'plugin2']);
		});
	});

	describe('#existing_plugins', function () {
		it('rejects on filesystem error', function () {
			const context = mock_context();
			context.fs.readdir = (dir, fn) => fn({code: 'EBAD'});
			const plugin_loader = new PluginLoader(context);
			return plugin_loader.existing_plugins().should.eventually.be.rejected;
		});

		it('returns no files if plugindir does not exist', async function () {
			const context = mock_context();
			context.fs.readdir = (dir, fn) => fn({code: 'ENOENT'});
			const plugin_loader = new PluginLoader(context);
			const result = await plugin_loader.existing_plugins();
			[...result].should.deep.equal([]);
		});

		it('removes koi from list of plugins', async function () {
			const context = mock_context();
			context.fs.readdir = (dir, fn) => fn(null, ['dir1', 'dir2', 'koi', 'dir3']);
			const plugin_loader = new PluginLoader(context);
			const result = await plugin_loader.existing_plugins();
			[...result].should.deep.equal(['dir1', 'dir2', 'dir3']);
		});
	});

	describe('#unregister_plugin', function () {
		it('cascades deletes to the versions table', async function () {
			const plugin_id =
				await context.db.run(
					'INSERT INTO Plugins (plugin_name, enabled) VALUES ("plugin", 1)');
			await context.db.run(
				'INSERT INTO Versions (plugin_id, version) VALUES ($plugin_id, -1)'
				, {$plugin_id: plugin_id});
			const plugin_loader = new PluginLoader(context);
			await plugin_loader.unregister_plugin('plugin');
			const all_plugins = await context.db.all('SELECT * FROM Plugins');
			const all_versions = await context.db.all('SELECT * FROM Versions');
			all_plugins.should.have.lengthOf(1);
			all_versions.should.have.lengthOf(1);
		});
	});

	describe('#register_plugin', function () {
		it('successfully registers a plugin', async function () {
			const plugin_loader = new PluginLoader(context);
			await plugin_loader.register_plugin('plugin');
			const all_plugins =
				await context.db.all(
					'SELECT Plugins.plugin_id, plugin_name, enabled, version '
					+ 'FROM Plugins, Versions '
					+ 'WHERE Plugins.plugin_id = Versions.plugin_id');
			all_plugins.last().should.deep.equal({
				plugin_id: 1, plugin_name: 'plugin', enabled: 1, version: 0});
		});
	});

	describe('#register', function () {
		it('registers and unregisters appropriately', async function () {
			const context = mock_context();
			context.db.all = mock_resolves(() =>
				[{plugin_name: 'not_exists'}, {plugin_name: 'exists'}]);
			context.db.run = mock_resolves(() => 42);
			context.fs.readdir = mock((dir, fn) => fn(null, ['exists', 'unregistered']));
			const plugin_loader = new PluginLoader(context);
			const actions = await plugin_loader.register();
			actions.should.deep.equal([42, 42]);
			context.db.all.invocations().should.equal(1);
			context.db.run.invocations().should.equal(3);
			context.db.run.last_invocation_args().should.deep.equal(
				['INSERT INTO Versions (plugin_id, version) VALUES ($plugin_id, $version)'
				, {$plugin_id: 42, $version: 0}]);
			context.fs.readdir.invocations().should.equal(1);
		});
	});

	describe('#update', function () {
		it('fails if plugins are unavailable', async function () {
			const context = mock_context();
			const plugin_loader = new PluginLoader(context);
			context.db.all = mock_rejects(() => {return {message: 'bad'}});
			const actions = await plugin_loader.upgrade();
			actions.should.have.lengthOf(0);
			context.log.error.last_invocation_args().should.deep.equal([
				'Failed to retrieve list of enabled plugins: bad']);
		});

		it('migrates out-of-date plugins', async function () {
			const context = mock_context();
			const plugin_loader = new PluginLoader(context);

			context.path.join = (...args) => args.join('/');
			context.config.plugindir = 'plugindir';
			context.db.migrate = mock_resolves((id, schemadir) => [id, schemadir]);
			context.db.all = mock_resolves(() => [
				{plugin_id: 1, plugin_name: 'a'}
				, {plugin_id: 2, plugin_name: 'b'}
			]);

			const actions = await plugin_loader.upgrade();
			actions.should.deep.equal([[1, 'plugindir/a/schema'], [2, 'plugindir/b/schema']]);
		});
	});

	describe('#load', function () {
		it('fails if plugins are unavailable', async function () {
			const context = mock_context();
			const plugin_loader = new PluginLoader(context);
			context.db.all = mock_rejects(() => {return {message: 'bad'}});
			const plugin_map = await plugin_loader.load();
			plugin_map.size.should.equal(0);
			context.log.error.last_invocation_args().should.deep.equal([
				'Failed to retrieve list of enabled plugins: bad']);
		});

		it('loads enabled plugins', async function () {
			const context = mock_context();
			const plugin_loader = new PluginLoader(context);
			context.path.join = (...args) => args.join('/');
			context.config.plugindir = 'plugindir';
			context.require = path => function () {this.path = path; this.enabled = false;};
			context.db.all = mock_resolves(() => [{plugin_name: 'a'}, {plugin_name: 'b'}]);
			const plugin_map = await plugin_loader.load();
			context.plugins.map(plugin => plugin.path).should.deep.equal([
				'./plugindir/a/plugin'
				, './plugindir/b/plugin']);
			plugin_map.size.should.equal(2);
			plugin_map.get('a').should.deep.equal({path: './plugindir/a/plugin', enabled: true});
			plugin_map.get('b').should.deep.equal({path: './plugindir/b/plugin', enabled: true});
		});

		it('loads plugin configs', async function () {
			const context = mock_context();
			const plugin_loader = new PluginLoader(context);
			context.path.join = (...args) => args.join('/');
			context.config.plugindir = 'plugindir';
			context.db.all = mock_resolves(() => [{plugin_name: 'a'}]);

			let invocations = 0;
			context.require = () => {
				if (invocations < 1) {
					invocations++;
					return {key1: 'val1', key2: 'val2'};
				} else {
					return function (...args) {this.args = args;}
				}
			};

			const plugin_map = await plugin_loader.load();
			plugin_map.get('a').should.deep.equal({args: [context, {key1: 'val1', key2: 'val2'}]});
		});
	});
});
