'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock = require('./mock').mock;
const mock_context = require('./context.mock');
const Database = require('../lib/Database');

describe('Database', function () {
	describe('#order_files_in_dir', function () {
		it('fails on error', function () {
			const context = mock_context();
			context.fs.readdir = (dir, fn) => fn(new Error('error'));
			const db = new Database(context);
			return db.order_files_in_dir().should.eventually.be.rejected;
		});

		it('orders files numerically', function () {
			const context = mock_context();
			context.fs.readdir = (dir, fn) =>
				fn(null, ['v10.sql', 'v0.sql', 'v1.sql', 'v999.sql', 'v99.sql']);
			const db = new Database(context);
			return db.order_files_in_dir().should.eventually.deep.equal(
				['v0.sql', 'v1.sql', 'v10.sql', 'v99.sql', 'v999.sql']);
		});
	});

	describe('#run_queries_from_file', function () {
		it('fails on filesystem error', function () {
			const context = mock_context();
			context.fs.readFile = (file, encoding, fn) => fn(new Error('error'));
			const db = new Database(context);
			return db.run_queries_from_file().should.eventually.be.rejected;
		});

		it('fails on database error', async function () {
			const context = mock_context();
			context.fs.readFile = mock((file, encoding, cb) => cb(null, 'contents'));
			context.raw_db.exec = (data, fn) => fn(new Error('error'));
			const db = new Database(context);
			try {
				await db.run_queries_from_file();
			} catch (err) {
				err.message.should.equal('error');
				context.fs.readFile.invocations().should.equal(1);
			}
		});

		it('execs the whole file', async function () {
			const context = mock_context();
			context.fs.readFile = mock((file, encoding, fn) => fn(null, 'query1; query2; \n'));
			const db = new Database(context);
			await db.run_queries_from_file();
			context.fs.readFile.invocations().should.equal(1);
			context.raw_db.exec.invocations().should.equal(2);
			context.raw_db.exec.last_invocation_args().slice(0, 1).should.deep.equal(['query2']);
		});
	});

	describe('#run', function () {
		it('provides lastID', async function () {
			const context = mock_context();
			context.raw_db.run = (sql, params, fn) => fn.apply({lastID: 42});
			const db = new Database(context);
			const result = await db.run();
			result.should.equal(42);
		});
	});

	describe('#setup', function () {
		it('fails when unable to add koi plugin', async function () {
			const context = mock_context();
			context.fs.readFile = (file, encoding, fn) => fn(null, 'query');
			context.fs.readdir = mock((dir, fn) => fn(null, []));
			context.raw_db.run = (sql, params, fn) => fn(new Error('error'));
			const db = new Database(context);

			try {
				await db.setup();
			} catch (e) {
				context.log.error.last_invocation_args().should.deep.equal(
					['Failed to add koi plugin: error']);
			}
		});

		it('sets up basic tables and adds the koi plugin', async function () {
			const context = mock_context();
			context.fs.readFile = mock((file, encoding, fn) => fn(null, 'query'));
			context.raw_db.run = mock((sql, params, fn) => fn.apply({}));
			const db = new Database(context);
			await db.setup();
			context.path.join.invocations().should.equal(1);
			context.fs.readFile.invocations().should.equal(1);
			context.raw_db.exec.invocations().should.equal(1);
			context.raw_db.run.invocations().should.equal(2);
		});
	});

	describe('#backup', function () {
		it('copies the database file', function () {
			const context = mock_context();
			context.config.dbfile = 'test.db';
			context.util.copy_file = mock((context, from, to) => {});
			const db = new Database(context);
			db.backup();
			context.util.copy_file.last_invocation_args().should.deep.equal(
				[context, 'test.db', 'test.db.bak']);
		});
	});

	describe('#rollback', function () {
		it('replaces the regular database with the backup', async function () {
			const context = mock_context();
			context.config.dbfile = 'test.db';
			context.util.copy_file = mock((context, from, to) => {});
			const db = new Database(context);
			await db.rollback();
			context.raw_db.close.invocations().should.equal(1);
			context.util.copy_file.last_invocation_args().should.deep.equal(
				[context, 'test.db.bak', 'test.db']);
		});
	});

	describe('#migrate', function () {
		it('migrates a plugin to the latest version', async function () {
			const context = mock_context();
			context.raw_db.get = (sql, params, cb) => cb(null, {version: 0});
			context.fs.readFile = (file, encoding, cb) => cb(null, 'query');
			context.fs.readdir = (dir, cb) => cb(null, ['v100.sql']);
			const db = new Database(context);
			await db.migrate(9);
			context.raw_db.exec.invocations().should.equal(1);
			context.raw_db.run.last_invocation_args().slice(0, 2).should.deep.equal([
				'UPDATE Versions SET version = $version WHERE plugin_id = $plugin_id'
				, {$version: 100, $plugin_id: 9}
			]);
		});
	});

	describe('#insert', function () {
		it('generates correct insert statements', async function () {
			const context = mock_context();
			const db = new Database(context);
			await db.insert('tbl', {col1: 'a', col2: 'b'});
			context.raw_db.run.last_invocation_args().slice(0, 2).should.deep.equal([
				'INSERT INTO tbl (col1, col2) VALUES ($col1, $col2)'
				, {$col1: 'a', $col2: 'b'}
			]);
		});
	});

	describe('#insert_batch', function () {
		it('generates correct insert statements', async function () {
			const context = mock_context();
			const db = new Database(context);
			await db.insert_batch('tbl', [{col1: 'a', col2: 'b'}, {col1: 'c', col2: 'd'}]);
			context.raw_db.run.last_invocation_args().slice(0, 2).should.deep.equal([
				'INSERT INTO tbl (col1, col2) VALUES ($0_col1, $0_col2), ($1_col1, $1_col2)'
				, {$0_col1: 'a', $0_col2: 'b', $1_col1: 'c', $1_col2: 'd'}
			]);
		});
	});
});
