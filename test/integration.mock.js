'use strict';

const stub = require('./mock').stub;

module.exports = async () => {
	const context = {
		client: {
			on: stub()
			, login: stub()
			, user: {id: 'koibot'}
		}
		, config: {}
		, discord: require('discord.js')
		, fs: require('fs')
		, log: {
			error: stub()
			, info: stub()
			, warn: stub()
		}
		, mkdirp: require('mkdirp')
		, moment: require('moment')
		, path: require('path')
		, plugins: []
		, require: stub()
		, sqlite: require('sqlite3')
		, util: require('../lib/util')
	};

	const Database = require('../lib/Database');
	const EventEmitter = require('events');
	const PluginLoader = require('../lib/PluginLoader');
	context.db = new Database(context);
	context.events = new EventEmitter();
	context.plugin_loader = new PluginLoader(context);

	return new Promise(resolve => {
		context.raw_db = new context.sqlite.Database(':memory:', () => {
			context.raw_db.serialize();
			context.raw_db.run('PRAGMA foreign_keys = ON;', async () => {
				await context.db.setup();
				await context.db.migrate(0, 'schema');
				resolve(context);
			});
		});
	});
};
