'use strict';

require('chai').use(require('chai-as-promised')).should();

const mock_context = require('./context.mock');
const Logger = require('../lib/Logger');

describe('Logger', function () {
	it('writes to stdout', function () {
		const context = mock_context();
		const log = new Logger(context);
		log.warn('warning');
		context.stdout.last_invocation_args().should.deep.equal(['[ISO8601] [WARN ] warning']);
		context.stderr.invocations().should.equal(0);
	});

	it('writes to stderr', function () {
		const context = mock_context();
		const log = new Logger(context);
		log.error('error');
		context.stderr.invocations().should.equal(1);
		context.stdout.invocations().should.equal(0);
	});

	it('creates directories to the given log file', function () {
		const context = mock_context();
		context.config.logfile = 'koi.log';
		new Logger(context);
		context.mkdirp.sync.invocations().should.equal(1);
		context.path.dirname.invocations().should.equal(1);
	});

	it('logs an error when unable to create directories', function () {
		const context = mock_context();
		context.config.logfile = 'koi.log';
		context.mkdirp.sync = () => {
			throw new Error('error');
		};

		new Logger(context);
		context.stderr.last_invocation_args().should.deep.equal([
			'[ISO8601] [ERROR] Failed to create path to log file: error'
		]);
	});

	it('logs an error when unable to append to log file', async function () {
		const context = mock_context();
		context.config.logfile = 'koi.log';
		context.fs.appendFile = (path, data, opts, cb) => cb(new Error('error'));

		const log = new Logger(context);
		await log.info('test');
		context.stderr.last_invocation_args().should.deep.equal([
			'[ISO8601] [ERROR] Failed to write to log file: error'
		]);
	});

	it('logs to the logfile', async function () {
		const context = mock_context();
		context.config.logfile = 'koi.log';
		const log = new Logger(context);
		await log.info('test');
		context.fs.appendFile.last_invocation_args().slice(0, 3).should.deep.equal(
			['koi.log', '[ISO8601] [INFO ] test\n', {mode: 0o664}]);
	});
});
