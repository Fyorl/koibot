'use strict';

const EventEmitter = require('events');
const mock = require('./mock').mock;
const stub = require('./mock').stub;

module.exports = () => {
	return {
		client: {
			on: stub()
			, user: {id: 'koibot'}
		}
		, config: {
			logfile: false
		}
		, db: {
			all: stub()
			, run: stub()
		}
		, events: new EventEmitter()
		, fs: {
			appendFile: mock((path, data, opts, cb) => cb())
			, readFile: mock((file, encoding, cb) => cb())
		}
		, log: {
			error: stub()
			, info: stub()
		}
		, mkdirp: {
			sync: stub()
		}
		, moment: {
			utc: () => {
				return {
					format: () => 'ISO8601'
				};
			}
		}
		, path: {
			dirname: stub()
			, join: stub()
		}
		, plugins: []
		, raw_db: {
			close: mock(cb => cb())
			, exec: mock((data, cb) => cb())
			, run: mock((sql, params, cb) => cb.apply({}))
		}
		, require: stub()
		, stdout: stub()
		, stderr: stub()
		, util: {
			copy_file: stub()
		}
	};
};
