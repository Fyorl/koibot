A Discord chat bot for KoY.

# Dependencies

1. Node.js 15

# Setup

1. Create your bot application [here](https://discordapp.com/developers/applications/me) and mark it as a bot user.
2. Copy `config.example.json` to `config.json` and copy your bot's token to the appropriate place in `config.json`.
3. Visit https://discordapp.com/api/oauth2/authorize?client_id=COPY_CLIENT_ID_HERE&scope=bot&permissions=2080898166 to add your bot to your server.

# Running

Compile:
```bash
$ yarn install
```

Run:
```bash
$ node koibot.js
```
