CREATE TABLE Blacklist (
	blacklist_id INTEGER PRIMARY KEY,
	command TEXT NOT NULL,
	dc_user_id TEXT NOT NULL
);
