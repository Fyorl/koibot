'use strict';

module.exports = function (context) {
	this.enabled = false;
	this.help = {
		blacklist: [
			'!blacklist add **@user** *command1* *command2* ... - Blacklists the **@user** '
				+ 'and prevents them from invoking the specified *command*s.'
			, '!blacklist remove **@user** *command1* *command2* ... - '
				+ 'Reinstates the **@user**\'s ability to invoke the specified *command*s.'
		]
	};

	this.is_blacklisted = async (msg, cmd) => {
		try {
			const row =
				await context.db.get(
					'SELECT COUNT(*) FROM Blacklist WHERE dc_user_id = $user_id AND command = $cmd'
					, {$user_id: msg.author.id, $cmd: cmd});

			return row['COUNT(*)'] > 0;
		} catch (err) {
			context.log.error(`Failed to determine if user was blacklisted: ${err.message}`);
			return false;
		}
	};

	const handle_blacklist = (msg, line, args) => {
		if (!this.enabled || !context.config.is_admin(msg)) {
			return;
		}

		if (args.length < 3) {
			return;
		}

		if (args[0].hasOwnProperty('text')
			&& args[2].hasOwnProperty('text')
			&& args[1].hasOwnProperty('type')
			&& args[1].type === 'user')
		{
			if (args[0].text === 'add') {
				return add_to_blacklist(msg, args);
			} else if (args[0].text === 'remove') {
				return remove_from_blacklist(msg, args);
			}
		}
	};

	const add_to_blacklist = async (msg, args) => {
		try {
			const commands = args.slice(2);
			const rows = commands.map(cmd => {
				return {command: cmd.text, dc_user_id: args[1].id}
			});

			await context.db.insert_batch('Blacklist', rows);
			msg.channel.send(
				`<@${args[1].id}> blacklisted from the following commands: `
				+ `${commands.map(cmd => `*${cmd.text}*`).join(', ')}`);
		} catch (err) {
			context.log.error(`Failed to add user to blacklist: ${err.message}`);
		}
	};

	const remove_from_blacklist = async (msg, args) => {
		try {
			const commands = args.slice(2);
			const params = {$user_id: args[1].id};
			commands.forEach((cmd, i) => params[`\$${i}_cmd`] = cmd.text);

			const sql =
				`DELETE FROM Blacklist WHERE dc_user_id = $user_id AND command IN (`
				+ `${Object.keys(params).filter(k => k.endsWith('_cmd')).join(', ')})`;

			await context.db.run(sql, params);
			msg.channel.send(
				`<@${args[1].id}> removed from blacklist for the following commands: `
				+ `${commands.map(cmd => `*${cmd.text}*`).join(', ')}`);
		} catch (err) {
			context.log.error(`Failed to remove user from blacklist: ${err.message}`);
		}
	};

	context.events.on('command_blacklist', handle_blacklist);
};
