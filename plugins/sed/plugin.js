'use strict';

module.exports = function (context, custom_config) {
	this.enabled = false;
	this.help = {
		sed: [
			'!sed s/*find*/*replace*/*flags* - Searches recent messages with the *find* regular '
			+ 'expression (respecting the *flags*), and outputs a new message with the '
			+ '\'correction\' created by applying the *replace* regular expression to it.'
		]
	};

	const default_config = {
		lookback_lines: 200
	};

	const config = Object.override(default_config, custom_config);

	// @visible_for_testing
	this.handle_command = async (msg, line, args, cmd) => {
		if (!this.enabled || await context.util.is_blacklisted(context, msg, cmd)) {
			return;
		}

		const [find, replace, flags] = parse_sed(line);
		const find_regex = new RegExp(find, flags);
		const [author, found_msg] = await log_search(msg, find_regex);

		if (author !== undefined && found_msg !== undefined) {
			const new_msg = found_msg.replace(find_regex, replace);
			msg.channel.send(`<@${author}> FTFY: "${new_msg}"`);
		}
	};

	const log_search = async (msg, needle) => {
		const sql =
			'SELECT * FROM Messages WHERE dc_channel_id = $chan_id AND dc_author_id <> $bot_id '
			+ `ORDER BY time DESC LIMIT ${config.lookback_lines}`;
		const params = {$chan_id: msg.channel.id, $bot_id: context.client.user.id};

		try {
			const rows = await context.db.all(sql, params);
			for (const row of rows) {
				if (context.config.command_symbols.indexOf(row.content_raw[0]) < 0
					&& needle.test(row.content_raw))
				{
					return [row.dc_author_id, row.content_raw];
				}
			}
		} catch (err) {
			context.log.error(`Failed to run log search query: ${err.message}`);
		}

		return [];
	};

	const parse_sed = expr => {
		let buffer = '';
		let in_find = false;
		let in_replace = false;
		let in_flags = false;
		let escape_next = false;
		let find = '';
		let replace = '';
		let flags = '';

		for (let i = 0; i < expr.length; i++) {
			const c = expr[i];
			if (c === '\\' && !escape_next && ['/', '\\'].includes(expr[i + 1])) {
				escape_next = true;
				continue;
			}

			if (c === '/' && !escape_next) {
				if (in_replace) {
					replace = buffer;
					in_replace = false;
					in_flags = true;
				} else if (in_find) {
					find = buffer;
					in_find = false;
					in_replace = true;
				} else {
					in_find = true;
				}

				buffer = '';
				continue;
			}

			buffer += c;
			escape_next = false;
		}

		if (buffer.length > 0) {
			if (in_replace) {
				replace = buffer;
			} else if (in_flags) {
				flags = buffer;
			}
		}

		return [find, replace, flags];
	};

	context.events.on('command_sed', this.handle_command);
};
