'use strict';

module.exports = function (context) {
	this.enabled = false;

	// @visible_for_testing
	this.handle_message = message => {
		if (!this.enabled
			|| message.content.length < 1
			|| context.config.command_symbols.indexOf(message.content[0]) < 0)
		{
			return;
		}

		const content = message.content.substr(1);
		const args = this.parse_args(content);

		for (const arg of args) {
			let collection = new Map();

			switch (arg.type) {
				case 'user': collection = message.mentions.users; break;
				case 'role': collection = message.mentions.roles; break;
			}

			if (arg.id && collection.get(arg.id)) {
				arg.data = collection.get(arg.id);
			}
		}

		if (args.length > 0) {
			const cmd = args.shift();
			if (cmd.text) {
				context.events.emit(
					`command_${cmd.text}`
					, message
					, content.substr(cmd.text.length + 1)
					, args
					, cmd.text);
			}
		}
	};

	this.parse_args = (content, until = Infinity) => {
		const args = [];
		const digits = '0123456789'.split('');
		const mention_chars = '@&#!'.split('');

		let buffer = '';
		let in_quotes = false;
		let escape_next = false;
		let in_mention = false;
		let mention_type = 'user';

		for (const c of content) {
			if (args.length >= until) {
				buffer += c;
				continue;
			}

			if (in_mention) {
				if (buffer.length < 1 && mention_chars.indexOf(c) > -1) {
					switch (c) {
						case '@': case '!': mention_type = 'user'; break;
						case '#': mention_type = 'chan'; break;
						case '&': mention_type = 'role'; break;
					}

					continue;
				} else if (digits.indexOf(c) < 0 && c !== '>') {
					buffer = '<';
					in_mention = false;
				} else if (c === '>') {
					in_mention = false;
					args.push(new MentionArgument(mention_type, buffer));
					buffer = '';
					continue;
				}
			}

			if (c === '\\' && !escape_next) {
				escape_next = true;
				continue;
			}

			if (c === '<' && !escape_next && !in_quotes && buffer.length < 1) {
				in_mention = true;
				continue;
			}

			if (c === '"' && !escape_next) {
				in_quotes = !in_quotes;
				continue;
			}

			if (c === ' ' && !in_quotes && !escape_next) {
				if (buffer.length > 0) {
					args.push(new TextArgument(buffer));
				}

				buffer = '';
				continue;
			}

			buffer += c;
			escape_next = false;
		}

		if (buffer.length > 0) {
			args.push(new TextArgument(buffer.trim()));
		}

		args.simplify = function () {
			return this.filter(arg => arg.text).map(arg => arg.text);
		};

		return args;
	};

	context.client.on('messageCreate', this.handle_message);
};

const TextArgument = function (text) {
	this.text = text;
};

const MentionArgument = function (type, id) {
	this.type = type;
	this.id = id;
	this.data = {};
};
