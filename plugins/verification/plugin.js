'use strict';

require('../../lib/util');

module.exports = function (context, custom_config) {
	this.enabled = false;
	this.help = {
		verifyme: [
			'!verifyme - Verify that you have read the rules and guidelines.'
		]
	};

	const default_config = {
		verified_role_id: false
	};

	const config = Object.override(default_config, custom_config);

	const handle_verify = async (msg, line) => {
		if (!this.enabled
			|| config.verified_role_id === false
			|| await context.util.is_blacklisted(context, msg, line))
		{
			return;
		}

		if (msg.guild && msg.guild.available) {
			const role = await msg.guild.roles.fetch(config.verified_role_id);
			const guild_member = await msg.guild.members.fetch(msg.author.id);

			if (role && guild_member) {
				try {
					await guild_member.roles.add(role);
					msg.reply('Verified.');
				} catch (err) {
					context.log.error(`Failed to verify user: ${err.message}`);
				}
			}
		}
	};

	context.events.on('command_verifyme', handle_verify);
};
