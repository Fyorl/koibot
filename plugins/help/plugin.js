'use strict';

module.exports = function (context) {
	this.enabled = false;
	this.help = {
		help: [
			'!help - Print a list of available commands.'
			, '!help *command* - Print detailed usage instructions for the specific *command*.'
		]
	};

	const handle_command = (msg, line, args) => {
		if (!this.enabled) {
			return;
		}

		if (args.length < 1) {
			print_all_commands(msg);
		} else {
			print_help_for(msg, args.simplify()[0]);
		}
	};

	const print_all_commands = msg => {
		const cmd_char = context.config.command_symbols[0];
		const available_commands = new Set();

		for (const plugin of context.plugins) {
			if (plugin.hasOwnProperty('help')) {
				for (const cmd in plugin.help) {
					if (plugin.help.hasOwnProperty(cmd)) {
						available_commands.add(cmd);
					}
				}
			}
		}

		msg.reply(
			`Use ${cmd_char}help **command** to get help on a specific command. `
			+ 'Currently available commands are: '
			+ Array.from(available_commands.values()).map(cmd => `**${cmd}**`).join(', '));
	};

	const print_help_for = (msg, cmd) => {
		const candidate_plugins =
			context.plugins
				.filter(plugin => plugin.hasOwnProperty('help'))
				.filter(plugin => plugin.help.hasOwnProperty(cmd));

		if (candidate_plugins.length > 0) {
			let help = '**Usage**:\n';
			help += candidate_plugins[0].help[cmd].map(usage => {
				if (usage.length > 0 && usage[0] === '!') {
					usage = context.config.command_symbols[0] + usage.substr(1);
				}

				return '\t' + usage;
			}).join('\n');

			msg.channel.send(help);
		}
	};

	context.events.on('command_help', handle_command);
};
