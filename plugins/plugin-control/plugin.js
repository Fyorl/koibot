'use strict';

module.exports = function (context) {
	this.enabled = false;

	// @visible_for_testing
	this.set_plugin_enablement = async (msg, args, enabled) => {
		if (!this.enabled || args.length < 1 || !args[0].text) {
			return;
		}

		if (!context.config.is_admin(msg)) {
			msg.channel.send('Access denied.');
			return;
		}

		const plugin_name = args[0].text;
		const db_enabled = enabled === true ? 1 : 0;
		const str_enabled = enabled === true ? 'enable' : 'disable';

		if (!enabled && context.config.required_plugins.indexOf(plugin_name) > -1) {
			msg.channel.send('This plugin is required and cannot be disabled.');
			return;
		}

		try {
			await context.db.run(
				'UPDATE Plugins SET enabled = $enabled WHERE plugin_name = $plugin_name'
				, {$enabled: db_enabled, $plugin_name: plugin_name});

			const plugin = context.plugins.from_name(plugin_name);
			if (plugin && plugin.hasOwnProperty('enabled')) {
				plugin.enabled = enabled;
			}

			msg.channel.send(`Plugin '${plugin_name}' was ${str_enabled}d.`);
		} catch (err) {
			context.log.error(`Failed to ${str_enabled} plugin ${plugin_name}: ${err.message}`);
			msg.channel.send(`Failed to ${str_enabled} plugin '${plugin_name}'.`);
		}
	};

	const handle_enable = (msg, line, args) => {
		return this.set_plugin_enablement(msg, args, true);
	};

	const handle_disable = (msg, line, args) => {
		return this.set_plugin_enablement(msg, args, false);
	};

	context.events.on('command_enable-plugin', handle_enable);
	context.events.on('command_disable-plugin', handle_disable);
};
