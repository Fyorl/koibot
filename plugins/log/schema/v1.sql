CREATE TABLE Messages (
	message_id INTEGER PRIMARY KEY,
	dc_message_id TEXT UNIQUE NOT NULL,
	dc_channel_id TEXT NOT NULL,
	channel_name TEXT NULL,
	dc_server_id TEXT NULL,
	server_name TEXT NULL,
	content_raw TEXT NOT NULL,
	content_clean TEXT NOT NULL,
	dc_author_id TEXT NOT NULL,
	direct BOOLEAN NOT NULL DEFAULT 0,
	time INTEGER NOT NULL
);

CREATE TABLE Mentions (
	mention_id INTEGER PRIMARY KEY,
	message_id INTEGER NOT NULL,
	dc_mention_id TEXT NOT NULL,
	type TEXT NOT NULL DEFAULT 'user',
	FOREIGN KEY (message_id) REFERENCES Messages(message_id) ON DELETE CASCADE
);
