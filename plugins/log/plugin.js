'use strict';

module.exports = function (context) {
	this.enabled = false;

	// @visible_for_testing
	this.handle_message = async message => {
		if (!this.enabled) {
			return;
		}

		const row = {
			dc_message_id: message.id
			, dc_author_id: message.author.id
			, dc_channel_id: message.channel.id
			, direct: message.channel.type === 'dm'
			, time: message.createdTimestamp
			, content_raw: message.content
			, content_clean: message.cleanContent
		};

		if (!row.direct) {
			row.channel_name = message.channel.name;
		}

		if (message.channel.guild) {
			row.dc_server_id = message.channel.guild.id;
			row.server_name = message.channel.guild.name;
		}

		const message_id = await context.db.insert('Messages', row);
		Array.from(message.mentions.users.keys()).forEach(id =>
			this.add_mention('user', message_id, id));
		Array.from(message.mentions.roles.keys()).forEach(id =>
			this.add_mention('role', message_id, id));
	};

	// @visible_for_testing
	this.add_mention = (type, message_id, mention_id) =>
		context.db.insert(
			'Mentions'
			, {message_id: message_id, dc_mention_id: mention_id, type: type});

	context.client.on('messageCreate', this.handle_message);
};
