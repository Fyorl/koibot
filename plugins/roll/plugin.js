'use strict';

module.exports = function (context) {
	this.enabled = false;
	this.help = {
		roll: [
			'!roll *expression* - Simulate a dice roll. Example dice *expression*s include: '
			+ '1d20 + 5; 1d2 + 1d3 + 1d4; 2d12 + 1d8 - 9.'
		]
	};

	const handle_command = async (msg, line, args, cmd) => {
		if (!this.enabled || await context.util.is_blacklisted(context, msg, cmd)) {
			return;
		}

		msg.reply(roll_dice(line));
	};

	const roll_dice = expression => {
		const rolls = parse_dice_expression(expression);
		const results = evaluate_rolls(rolls);

		let output = '';
		const total =
			results.reduce((acc, cur) => acc.concat(cur), []) // flatten
				   .reduce((acc, cur) => acc + cur, 0);       // sum

		for (const result of results) {
			if (result.length < 1) {
				continue;
			}

			const first = result[0];
			const sign = first < 0 ? '-' : '+';

			if (output.length < 1 && first < 0) {
				output += '-';
			} else if (output.length > 0) {
				output += ` ${sign} `;
			}

			if (result.length < 2) {
				output += `**${first < 0 ? first * -1 : first}**`;
			} else {
				output += `( ${result.map(n => `**${n < 0 ? n * -1 : n}**`).join(' + ')} )`;
			}
		}

		if (output.indexOf(' ') > -1) {
			output += ` = **${total}**`;
		}

		return output;
	};

	const evaluate_rolls = rolls => {
		return rolls.map(roll => {
			if (roll.hasOwnProperty('v')) {
				return [roll.v * roll.sign];
			} else {
				const results = [];
				for (let i = 0; i < roll.n; i++) {
					results.push(context.util.get_random_int(1, roll.d) * roll.sign);
				}

				return results;
			}
		});
	};

	const parse_dice_expression = expression => {
		const rolls = [];
		let prev_sign = 1;
		let buf = '';

		const parse_term = () => {
			const split = buf.split('d');
			let term;

			if (split.length === 1) {
				const v = parseInt(split[0]);
				if (!isNaN(v)) {
					term = new Constant(prev_sign, v);
				}
			} else if (split.length === 2) {
				const n = parseInt(split[0]);
				const d = parseInt(split[1]);
				if (!isNaN(n) && !isNaN(d)) {
					term = new DiceRoll(prev_sign, n, d);
				}
			}

			if (term !== undefined) {
				rolls.push(term);
			}

			buf = '';
		};

		for (const c of expression) {
			if (c === '+') {
				parse_term();
				prev_sign = 1;
				continue;
			}

			if (c === '-') {
				parse_term();
				prev_sign = -1;
				continue;
			}

			if (c === ' ') {
				parse_term();
				continue;
			}

			buf += c;
		}

		parse_term();
		return rolls;
	};

	context.events.on('command_roll', handle_command);
	context.events.on('command_r', handle_command);
};

const DiceRoll = function (sign, n, d) {
	this.sign = sign;
	this.n = n;
	this.d = d;
};

const Constant = function (sign, v) {
	this.sign = sign;
	this.v = v;
};
