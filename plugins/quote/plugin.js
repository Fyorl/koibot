'use strict';

require('../../lib/util');

module.exports = function (context, custom_config) {
	this.enabled = false;
	this.help = {
		quote: [
			'!quote - Quotes the most recent line.'
			, '!quote *search* - '
				+ 'Quotes the most recent line that contains the whole *search* phrase.'
			, '!quote **@user** - Quotes the most recent line said by **@user**.'
			, '!quote **@user** *search* - '
				+ 'Quotes the most recent line said by **@user** '
				+ 'that contains the whole *search* phrase.'
		]
		, findquote: [
			'!findquote - Retrieves a random quote.'
			, '!findquote *#ID* - Retrieves the quote with *ID*.'
			, '!findquote *search* - '
				+ 'Retrieves a random quote that contains the whole *search* phrase.'
			, '!findquote **@user** - Retrieves a random quote by **@user**.'
			, '!findquote **@user** *search* - '
				+ 'Retrieves a random quote by **@user** that contains the whole *search* phrase.'
		]
		, randomquote: [
			'!randomquote - Retrieves a random quote.'
			, '!randomquote *search* - '
				+ 'Retrieves a random quote that contains the whole *search* phrase'
			, '!randomquote **@user** - Retrieves a random quote by **@user**.'
			, '!randomquote **@user** *search* - '
				+ 'Retrieves a random quote by **@user** that contains the whole *search* phrase.'
		]
		, removequote: [
			'!removequote *#ID* - '
				+ 'Removes the quote with *ID* if you originally quoted it or '
				+ 'if you are a bot administrator.'
		]
	};

	const default_config = {
		lookback_lines: 200
	};

	const config = Object.override(default_config, custom_config);

	// @visible_for_testing
	this.handle_quote = async (msg, line, args, cmd) => {
		if (!this.enabled || await context.util.is_blacklisted(context, msg, cmd)) {
			return;
		}

		let user_id;
		if (args.length > 0 && args[0].type && args[0].type === 'user') {
			user_id = args[0].id;
			line = line.substr(args[0].id.length + 4);
		}

		const new_quote_id = await quote_search(msg, line, user_id);

		if (new_quote_id > -1) {
			msg.channel.send(`Added quote #${new_quote_id}.`);
		}
	};

	// @visible_for_testing
	this.handle_findquote = async (msg, line, args, cmd) => {
		if (!this.enabled || await context.util.is_blacklisted(context, msg, cmd)) {
			return;
		}

		if (args.length > 0 && args[0].text && args[0].text[0] === '#') {
			const quote_id = parseInt(args[0].text.substr(1));
			if (!isNaN(quote_id)) {
				return this.display_quote(msg, await find_quote_by_id(quote_id));
			}
		} else {
			return this.handle_randomquote(msg, line, args, cmd);
		}
	};

	// @visible_for_testing
	this.handle_randomquote = async (msg, line, args, cmd) => {
		if (!this.enabled || !msg.guild || await context.util.is_blacklisted(context, msg, cmd)) {
			return;
		}

		let user_id;
		if (args.length > 0 && args[0].type && args[0].type === 'user') {
			user_id = args[0].id;
			line = line.substr(args[0].id.length + 4);
		}

		return this.display_quote(msg, await this.get_random_quote(msg, user_id, line));
	};

	// @visible_for_testing
	this.handle_removequote = async (msg, line, args, cmd) => {
		if (!this.enabled
			|| args.length < 1
			|| !args[0].text
			|| await context.util.is_blacklisted(context, msg, cmd))
		{
			return;
		}

		let quote_id = args[0].text;
		if (quote_id[0] === '#') {
			quote_id = quote_id.substr(1);
		}

		quote_id = parseInt(quote_id);
		if (context.config.is_admin(msg)) {
			await remove_quote(msg, quote_id);
		} else if (await owns_quote(msg.author.id, quote_id)) {
			await remove_quote(msg, quote_id);
		} else {
			msg.channel.send('Access denied.');
		}
	};

	const create_log_query = (limit, chan_id, user_id) => {
		let where = 'WHERE dc_channel_id = $chan_id AND dc_author_id <> $bot_id';
		const params = {$chan_id: chan_id, $bot_id: context.client.user.id};

		if (user_id) {
			where += ' AND dc_author_id = $user_id';
			params['$user_id'] = user_id;
		}

		const sql = `SELECT * FROM Messages ${where} ORDER BY time DESC LIMIT ${limit}`;
		return [sql, params];
	};

	const quote_search = async (msg, needle, user_id) => {
		const [sql, params] = create_log_query(config.lookback_lines, msg.channel.id, user_id);

		try {
			const rows = await context.db.all(sql, params);
			for (const row of rows) {
				if (context.config.command_symbols.indexOf(row.content_raw[0]) < 0
					&& row.content_raw.indexOf(needle) > -1)
				{
					return save_quote(msg, row);
				}
			}
		} catch (err) {
			context.log.error(
				`Failed to run quote search query (user_id: ${user_id}): ${err.message}`);
		}

		return -1;
	};

	const save_quote = async (msg, log_row) => {
		try {
			const quote_row = {
				dc_message_id: log_row.dc_message_id
				, dc_channel_id: log_row.dc_channel_id
				, channel_name: log_row.channel_name
				, dc_server_id: log_row.dc_server_id
				, server_name: log_row.server_name
				, content_raw: log_row.content_raw
				, content_clean: log_row.content_clean
				, dc_quoter_id: msg.author.id
				, dc_quotee_id: log_row.dc_author_id
				, time_posted: log_row.time
				, time_quoted: msg.createdTimestamp
				, direct: log_row.direct
			};

			const quote_id = await context.db.insert('Quotes', quote_row);
			context.db.run(
				'INSERT INTO QuoteMentions (dc_mention_id, type, quote_id) '
				+ 'SELECT dc_mention_id, type, $quote_id as quote_id FROM Mentions '
				+ 'WHERE message_id = $message_id'
				, {$quote_id: quote_id, $message_id: log_row.message_id});

			return quote_id;
		} catch (err) {
			if (err.message ===
				'SQLITE_CONSTRAINT: UNIQUE constraint failed: Quotes.dc_message_id')
			{
				msg.channel.send('Already quoted.');
			} else {
				context.log.error(`Failed to save quote: ${err.message}`);
			}
		}

		return -1;
	};

	const find_quote_by_id = async id => {
		try {
			return await context.db.get(
				'SELECT * FROM Quotes WHERE quote_id = $quote_id'
				, {$quote_id: id});
		} catch (err) {
			context.log.error(`Failed to retrieve quote #${id}: ${err.message}`);
		}
	};

	// @visible_for_monkey_patching
	this.get_random_quote = async (msg, user_id, needle) => {
		let where = 'WHERE dc_server_id = $server_id';
		const params = {$server_id: msg.guild.id};

		if (user_id) {
			where += ' AND dc_quotee_id = $user_id';
			params['$user_id'] = user_id;
		}

		if (needle && needle.length > 0) {
			where += ' AND content_raw LIKE $needle';
			params['$needle'] = `%${needle.replace(/%/g, '\\%')}%`;
		}

		try {
			const count = await context.db.get(`SELECT count(*) FROM Quotes ${where}`, params);
			if (count && count['count(*)'] && count['count(*)'] > 0) {
				const offset = context.util.get_random_int(0, count['count(*)'] - 1);
				return await context.db.get(
					`SELECT * FROM Quotes ${where} LIMIT 1 OFFSET ${offset}`
					, params);
			}
		} catch (err) {
			context.log.error(`Failed to retrieve random quote: ${err.message}`);
		}
	};

	// @visible_for_testing
	this.display_quote = async (msg, quote_row) => {
		try {
			const embed = new context.discord.EmbedBuilder();
			let quoter;
			let quotee;

			try {
				quoter = await msg.guild.members.fetch(quote_row.dc_quoter_id);
			} catch {
				context.log.error(`Unknown member: ${quote_row.dc_quoter_id}`);
			}

			try {
				quotee = await msg.guild.members.fetch(quote_row.dc_quotee_id);
			} catch {
				context.log.error(`Unknown member: ${quote_row.dc_quotee_id}`);
			}

			embed.setAuthor({
				name: quotee?.nickname ?? quotee?.user?.username ?? "Unknown",
				iconURL: quotee?.displayAvatarURL()
			});
			embed.setFooter({
				text: `Quoted by @${quoter?.nickname ?? quoter?.user?.username ?? "Unknown"}`,
				iconURL: quoter?.displayAvatarURL()
			});
			embed.setTitle(`#${quote_row.quote_id}`);
			embed.setDescription(quote_row.content_raw);
			embed.setTimestamp(context.moment.utc(quote_row.time_posted).toDate());
			msg.channel.send({embeds: [embed.toJSON()]});
		} catch (err) {
			context.log.error(`Failed to display quote: ${err.message}`);
		}
	};

	const owns_quote = async (user_id, quote_id) => {
		try {
			const quote =
				await context.db.get(
					'SELECT * FROM Quotes WHERE quote_id = $quote_id'
					, {$quote_id: quote_id});

			return quote && quote.dc_quoter_id === user_id;
		} catch (err) {
			context.log.error(`Failed to retrieve quote #${quote_id}: ${err.message}`);
		}

		return false;
	};

	const remove_quote = async (msg, quote_id) => {
		try {
			await context.db.run(
				'DELETE FROM Quotes WHERE quote_id = $quote_id'
				, {$quote_id: quote_id});
			msg.channel.send(`Removed quote #${quote_id}.`);
		} catch (err) {
			context.log.error(`Failed to remove quote #${quote_id}: ${err.message}`);
			msg.channel.send(`Failed to remove quote #${quote_id}.`);
		}
	};

	context.events.on('command_quote', this.handle_quote);
	context.events.on('command_findquote', this.handle_findquote);
	context.events.on('command_randomquote', this.handle_randomquote);
	context.events.on('command_removequote', this.handle_removequote);
};
