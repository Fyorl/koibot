CREATE TABLE Quotes (
	quote_id INTEGER PRIMARY KEY,
	dc_message_id TEXT UNIQUE NOT NULL,
	dc_channel_id TEXT NOT NULL,
	channel_name TEXT NULL,
	dc_server_id TEXT NULL,
	server_name TEXT NULL,
	content_raw TEXT NOT NULL,
	content_clean TEXT NOT NULL,
	dc_quoter_id TEXT NOT NULL,
	dc_quotee_id TEXT NOT NULL,
	time_posted INTEGER NOT NULL,
	time_quoted INTEGER NOT NULL,
	direct BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE QuoteMentions (
	quote_mention_id INTEGER PRIMARY KEY,
	quote_id INTEGER NOT NULL,
	dc_mention_id TEXT NOT NULL,
	type TEXT NOT NULL DEFAULT 'user',
	FOREIGN KEY (quote_id) REFERENCES Quotes(quote_id) ON DELETE CASCADE
);
