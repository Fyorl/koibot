'use strict';

const Logger = function (context) {
	const construct_line = (level, msg) => `[${context.moment.utc().format()}] [${level}] ${msg}`;
	const write = (level, msg) => {
		const line = construct_line(level, msg);
		if (level === 'ERROR') {
			context.stderr(line);
		} else {
			context.stdout(line);
		}

		if (context.config.logfile !== false) {
			return new Promise(resolve => {
				context.fs.appendFile(context.config.logfile, line + '\n', {mode: 0o664}, err => {
					if (err) {
						context.stderr(
							construct_line('ERROR', `Failed to write to log file: ${err.message}`));
					}

					resolve();
				});
			});
		} else {
			return Promise.resolve();
		}
	};

	this.error = msg => write('ERROR', msg);
	this.info = msg => write('INFO ', msg);
	this.warn = msg => write('WARN ', msg);

	if (context.config.logfile !== false) {
		try {
			context.mkdirp.sync(context.path.dirname(context.config.logfile), 0o775);
		} catch (err) {
			context.stderr(
				construct_line('ERROR', `Failed to create path to log file: ${err.message}`));
		}
	}
};

module.exports = Logger;
