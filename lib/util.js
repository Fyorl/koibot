'use strict';

exports.copy_file = (context, from, to) => new Promise((resolve, reject) => {
	let error_encountered = false;
	const reader = context.fs.createReadStream(from);
	const writer = context.fs.createWriteStream(to, {mode: 0o664, defaultEncoding: 'binary'});

	const fail = err => {
		error_encountered = true;
		reader.end();
		writer.end();
		reject(err);
	};

	reader.on('error', fail);
	writer.on('error', fail);
	writer.on('finish', () => {
		if (!error_encountered) {
			resolve();
		}
	});

	reader.pipe(writer);
});

// Converts e.g. 'v299.sql' to 299.
exports.version_number = filename => parseInt(filename.substring(1, filename.indexOf('.')));

Array.prototype.last = function () {
	return this[this.length - 1];
};

Promise.all_without_fail = iterable => Promise.all(iterable.map(p => p.catch(e => e)));

Object.override = (base, override) => {
	const result = {};

	for (const prop in base) {
		if (base.hasOwnProperty(prop)) {
			result[prop] = base[prop];
		}
	}

	if (override !== null && typeof override === 'object') {
		for (const prop in override) {
			if (override.hasOwnProperty(prop)) {
				result[prop] = override[prop];
			}
		}
	}

	return result;
};

exports.get_random_int = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

exports.is_blacklisted = async (context, msg, cmd) => {
	const blacklist_plugin = context.plugins.from_name('blacklist');
	if (blacklist_plugin === undefined) {
		return false;
	} else {
		return blacklist_plugin.is_blacklisted(msg, cmd);
	}
};
