'use strict';

const util = require('./util');
const Database = require('./Database');
const Logger = require('./Logger');
const PluginLoader = require('./PluginLoader');
const EventEmitter = require('events');

module.exports = function (context) {
	context.util = util;
	context.log = new Logger(context);
	context.db = new Database(context);
	context.plugin_loader = new PluginLoader(context);
	context.plugins = [];
	context.events = new EventEmitter();

	context.client.on('ready', () => context.log.info('[READY]'));
	context.client.on('error', err => context.log.error(`[ERROR] ${err.message}`));

	context.config.is_admin = function (msg) {
		return this.root_id === msg.author.id
			|| (msg.member && msg.member.roles.cache.has(this.admin_role_id));
	};

	this.connect = () => {
		context.log.info('Logging in...');
		context.client.login(context.config.token).then(() => context.log.info('[LOGIN]'));
	};

	this.initialise_db = () => {
		context.log.info('Initialising database...');
		try {
			context.mkdirp.sync(context.path.dirname(context.config.dbfile), 0o775);
		} catch (err) {
			context.log.error(`Unable to create database file: ${err.message}`);
			return Promise.reject();
		}

		return new Promise((resolve, reject) => {
			const db = new context.sqlite.Database(context.config.dbfile, err => {
				if (err) {
					context.log.error(`Unable to open database file: ${err.message}`);
					reject();
				} else {
					// Run all DB queries in serial. This may be bad for
					// performance but simplifies the code. Change this if it
					// becomes an issue in future.
					db.serialize();
					db.run('PRAGMA foreign_keys = ON;', async err => {
						if (err) {
							context.log.error(
								`Unable to activate database constraints: ${err.message}`);
							reject();
						} else {
							context.raw_db = db;
							try {
								const stats = context.fs.statSync(context.config.dbfile);
								if (stats.size < 1) {
									await context.db.setup();
								}

								resolve();
							} catch (err) {
								context.log.error(`Unable to initialise database: ${err.message}`);
								reject();
							}
						}
					});
				}
			});
		});
	};

	this.upgrade_db = async () => {
		context.log.info('Upgrading database...');
		try {
			await context.db.backup();
		} catch (err) {
			context.log.error(`Failed to create database backup: ${err.message}`);
			return Promise.reject();
		}

		try {
			await context.db.migrate(0, 'schema');
			return Promise.resolve();
		} catch (err) {
			context.log.error(`Migrating database failed (${err.message}), attempting rollback.`);
			try {
				await context.db.rollback();
			} catch (err) {
				context.log.error(`Rollback failed: ${err.message}`);
			}
		}

		return Promise.reject();
	};

	this.register_plugins = () => {
		context.log.info('Registering plugins...');
		return context.plugin_loader.register();
	};

	this.upgrade_plugins = async () => {
		context.log.info('Upgrading plugins...');
		try {
			await context.db.backup();
		} catch (err) {
			context.log.error(`Failed to create database backup: ${err.message}`);
			return Promise.reject();
		}

		return context.plugin_loader.upgrade();
	};

	this.load_plugins = async () => {
		context.log.info('Loading plugins...');
		const plugin_map = await context.plugin_loader.load();
		context.plugins.from_name = name => plugin_map.get(name);
		return Promise.resolve();
	};

	this.check_plugins = () => {
		for (const plugin_name of context.config.required_plugins) {
			const plugin = context.plugins.from_name(plugin_name);
			if (!plugin) {
				const msg = `Required plugin '${plugin_name}' was not loaded.`;
				context.log.error(msg);
				throw new Error(msg);
			}

			if (plugin.hasOwnProperty('enabled') && !plugin.enabled) {
				const msg = `Required plugin '${plugin_name}' is disabled.`;
				context.log.error(msg);
				throw new Error(msg);
			}
		}
	};
};
