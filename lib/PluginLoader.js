'use strict';

module.exports = function (context) {
	// @visible_for_testing
	this.registered_plugins = async () => {
		const rows = await context.db.all('SELECT * FROM Plugins WHERE plugin_id > 0');
		return new Set(rows.map(row => row.plugin_name));
	};

	// @visible_for_testing
	this.existing_plugins = () => new Promise((resolve, reject) => {
		context.fs.readdir(context.config.plugindir, (err, files) => {
			if (err) {
				if (err.code === 'ENOENT') {
					files = [];
				} else {
					reject(err);
					return;
				}
			}

			const koi_index = files.indexOf('koi');
			if (koi_index > -1) {
				files.splice(koi_index, 1);
			}

			resolve(new Set(files));
		});
	});

	// @visible_for_testing
	this.unregister_plugin = plugin =>
		context.db.run('DELETE FROM Plugins WHERE plugin_name = $plugin', {$plugin: plugin});

	// @visible_for_testing
	this.register_plugin = async plugin => {
		const plugin_id =
			await context.db.run(
				'INSERT INTO Plugins (plugin_name, enabled) VALUES ($plugin, $enabled)'
				, {$plugin: plugin, $enabled: 1});
		return context.db.run(
			'INSERT INTO Versions (plugin_id, version) VALUES ($plugin_id, $version)'
			, {$plugin_id: plugin_id, $version: 0});
	};

	this.register = async () => {
		const [registered, existing] =
			await Promise.all([this.registered_plugins(), this.existing_plugins()]);

		const actions = [];
		for (const plugin of registered) {
			if (!existing.has(plugin)) {
				actions.push(this.unregister_plugin(plugin));
			}
		}

		for (const plugin of existing) {
			context.log.info(`Plugin '${plugin}' found...`);
			if (!registered.has(plugin)) {
				actions.push(this.register_plugin(plugin));
			}
		}

		return Promise.all_without_fail(actions);
	};

	this.upgrade = async () => {
		const actions = [];
		let plugins = [];

		try {
			plugins =
				await context.db.all(
					'SELECT * FROM Plugins WHERE plugin_id > 0 AND enabled = 1');
		} catch (err) {
			context.log.error(`Failed to retrieve list of enabled plugins: ${err.message}`);
		}

		for (const plugin of plugins) {
			context.log.info(`Checking for updates to plugin '${plugin.plugin_name}'...`);
			const schemadir =
				context.path.join(context.config.plugindir, plugin.plugin_name, 'schema');
			actions.push(context.db.migrate(plugin.plugin_id, schemadir));
		}

		return Promise.all_without_fail(actions);
	};

	this.load = async () => {
		const plugin_map = new Map();
		let plugins = [];

		try {
			plugins =
				await context.db.all(
					'SELECT * FROM Plugins WHERE plugin_id > 0 AND enabled = 1');
		} catch (err) {
			context.log.error(`Failed to retrieve list of enabled plugins: ${err.message}`);
		}

		for (const plugin of plugins) {
			context.log.info(`Loading plugin '${plugin.plugin_name}...'`);

			let plugin_config;
			const plugin_config_path =
				'./' + context.path.join(
					context.config.plugindir
					, plugin.plugin_name
					, 'config.json');

			try {
				plugin_config = context.require(plugin_config_path);
			} catch (err) {
				// it's fine if a plugin has no config file.
			}

			const plugin_code =
				'./' + context.path.join(context.config.plugindir, plugin.plugin_name, 'plugin');
			const plugin_class = context.require(plugin_code);
			const plugin_obj = new plugin_class(context, plugin_config);

			if (plugin_obj.hasOwnProperty('enabled')) {
				plugin_obj.enabled = true;
			}

			context.plugins.push(plugin_obj);
			plugin_map.set(plugin.plugin_name, context.plugins.last());
		}

		return plugin_map;
	};
};
