'use strict';

const version_number = require('./util').version_number;

module.exports = function (context) {
	// @visible_for_testing
	this.order_files_in_dir = dir => new Promise((resolve, reject) => {
		context.fs.readdir(dir, (err, files) => {
			if (err) {
				reject(err);
			} else {
				resolve(files.sort((a, b) => {
					let a_num = version_number(a);
					let b_num = version_number(b);

					if (isNaN(a_num)) {
						return 1;
					}

					if (isNaN(b_num)) {
						return -1;
					}

					return a_num - b_num;
				}));
			}
		});
	});

	// @visible_for_testing
	this.run_queries_from_file = file => new Promise((resolve, reject) => {
		context.fs.readFile(file, 'utf8', (err, data) => {
			if (err) {
				reject(err);
			} else {
				const queries = data.split(';').map(datum => datum.trim());
				const run_query = () => {
					const query = queries.shift();
					if (query.length < 1) {
						if (queries.length > 0) {
							run_query();
							return;
						} else {
							resolve();
							return;
						}
					}

					context.raw_db.exec(query, err => {
						if (err) {
							reject(err);
						} else if (queries.length > 0) {
							run_query();
						} else {
							resolve();
						}
					});
				};

				run_query();
			}
		});
	});

	this.run = (sql, params) => new Promise((resolve, reject) => {
		context.raw_db.run(sql, params, function (err) {
			if (err) {
				reject(err);
			} else {
				resolve(this.lastID);
			}
		});
	});

	this.insert = (tbl, row) => {
		const sql =
			`INSERT INTO ${tbl} (${Object.keys(row).join(', ')}) `
			+ `VALUES (${Object.keys(row).map(k => `\$${k}`).join(', ')})`;

		const params = {};
		for (const col in row) {
			if (!row.hasOwnProperty(col)) {
				continue;
			}

			params[`\$${col}`] = row[col];
		}

		return this.run(sql, params);
	};

	this.insert_batch = (tbl, rows) => {
		const values =
			rows.map((row, i) =>
				`(${Object.keys(row).map(k => `\$${i}_${k}`).join(', ')})`)
				.join(', ');

		const sql = `INSERT INTO ${tbl} (${Object.keys(rows[0]).join(', ')}) VALUES ${values}`;

		const params = {};
		rows.forEach((row, i) => {
			for (const col in row) {
				if (!row.hasOwnProperty(col)) {
					continue;
				}

				params[`\$${i}_${col}`] = row[col];
			}
		});

		return this.run(sql, params);
	};

	this.get = (sql, params) => new Promise((resolve, reject) => {
		context.raw_db.get(sql, params, (err, row) => {
			if (err) {
				reject(err);
			} else {
				resolve(row);
			}
		});
	});

	this.exec = sql => new Promise((resolve, reject) => {
		context.raw_db.exec(sql, err => {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});

	this.all = (sql, params) => new Promise((resolve, reject) => {
		context.raw_db.all(sql, params, (err, rows) => {
			if (err) {
				reject(err);
			} else {
				resolve(rows);
			}
		});
	});

	this.close = () => new Promise((resolve, reject) => {
		context.raw_db.close(err => {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});

	this.setup = async () => {
		try {
			await this.run_queries_from_file(context.path.join('schema', 'v1.sql'));
			await this.run(
				'INSERT INTO Plugins (plugin_id, plugin_name, enabled) '
				+ 'VALUES ($plugin_id, $plugin_name, $enabled)'
				, {$plugin_id: 0, $plugin_name: 'koi', $enabled: 1});
			await this.run(
				'INSERT INTO Versions (plugin_id, version) VALUES ($plugin_id, $version)'
				, {$plugin_id: 0, $version: 1});
			return Promise.resolve();
		} catch (err) {
			context.log.error(`Failed to add koi plugin: ${err.message}`);
			return Promise.reject();
		}
	};

	this.backup = () =>
		context.util.copy_file(context, context.config.dbfile, `${context.config.dbfile}.bak`);

	this.rollback = async () => {
		await this.close();
		await context.util.copy_file(
			context
			, `${context.config.dbfile}.bak`
			, context.config.dbfile);
		return Promise.resolve();
	};

	this.migrate = async (plugin_id, migration_dir) => {
		const row = await this.get(
			'SELECT * FROM Versions WHERE plugin_id = $plugin_id'
			, {$plugin_id: plugin_id});
		const files = await this.order_files_in_dir(migration_dir);
		let latest_version;

		for (const file of files) {
			const file_version = version_number(file);
			if (row.version >= file_version) {
				continue;
			}

			await this.run_queries_from_file(context.path.join(migration_dir, file));
			latest_version = file_version;
		}

		if (latest_version > -1) {
			await this.run(
				'UPDATE Versions SET version = $version WHERE plugin_id = $plugin_id'
				, {$version: latest_version, $plugin_id: plugin_id});
		}

		return Promise.resolve();
	};
};
