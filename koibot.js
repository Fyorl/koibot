'use strict';

const Discord = require('discord.js');
const Koi = require('./lib/Koi');
const context = {};
const bits = Discord.GatewayIntentBits;

context.require = require;
context.client = new Discord.Client({
	intents: [
		bits.Guilds, bits.GuildMembers, bits.GuildMessages, bits.MessageContent
	]
});
context.config = require('./config.json');
context.discord = Discord;
context.fs = require('fs');
context.mkdirp = require('mkdirp');
context.moment = require('moment');
context.path = require('path');
context.sqlite = require('sqlite3');
context.stderr = console.error;
context.stdout = console.log;

async function main () {
	const koi = new Koi(context);

	try {
		await koi.initialise_db();
		await koi.upgrade_db();

		try {
			await koi.register_plugins();
			await koi.upgrade_plugins();
			await koi.load_plugins();
		} catch (err) {
			console.error(err.message);
		}

		koi.check_plugins();
		koi.connect();
	} catch (err) {
		console.error('Koibot has failed to start up, please check any previous errors.');
	}
}

process.on('uncaughtException', err => {
	if (context !== null
		&& context !== undefined
		&& Object.prototype.hasOwnProperty.call(context, 'log')
		&& Object.prototype.hasOwnProperty.call(context.log, 'error'))
	{
		context.log.error(`[ERROR] ${err.message}`);
	} else {
		console.error('Uncaught exception: ', err);
	}
});

//noinspection JSIgnoredPromiseFromCall
main();
